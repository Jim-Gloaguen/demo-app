# Stop using COBOL

* Status: rejected

## Context and Problem Statement

COBOL is fun to trash-talk but less fun to use. Let's just stop.

## Considered Options

* Stop using COBOL
* Use anything else that is from 2010+

## Decision Outcome

* If it is not broken, don't try to fix it